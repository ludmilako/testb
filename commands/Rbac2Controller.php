<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createBonus = $auth->createPermission('createBonus');
		$createBonus->description = 'Admin can create new Bonuses';
		$auth->add($createBonus);
		
		$updateBonus = $auth->createPermission('updateBonus');
		$updateBonus->description = 'Admin can update all Bonuses';
		$auth->add($updateBonus);

		$deleteBonus = $auth->createPermission('deleteBonus');
		$deleteBonus->description = 'Admin can delete Bonuses';
		$auth->add($deleteBonus);
	}

	
		
	


	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
				
		$admin = $auth->getRole('admin');
		
		$createBonus = $auth->getPermission('createBonus');
		$auth->addChild($admin, $createBonus);

		$updateBonus = $auth->getPermission('updateBonus');
		$auth->addChild($admin, $updateBonus);
		
		$deleteBonus = $auth->getPermission('deleteBonus');
		$auth->addChild($admin, $deleteBonus);		
	}
}