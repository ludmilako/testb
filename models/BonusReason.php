<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bonusReason".
 *
 * @property integer $id
 * @property string $name
 */
class BonusReason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonusReason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    
    
    
     public static function getBonusReasonsToWidget()
	{
		$allBonusReasons = self::find()->select(['id as value', 'name as label'])
            ->asArray()
            ->all();
		return $allBonusReasons;						
	}
        
	
	public static function getBonusReasonsWithAllBonusReasons()
	{
		$allBonusReason = self:: getBonusReasons();
		$allBonusReason[-1] = 'All BonusReasons';
		$allBonusReason = array_reverse ( $allBonusReason, true );
		return $allBonusReason;	
	}
	
	public static function getExistBonusReasonsWithAllBonusReasons()
	{
		//get all the BonusReasons id in the bonus table
		$bonuses = new bonus();
		$bonusesExist= $bonuses->find()->select('reasonId')->distinct()->all();
		$bonusesExistArr = [];
		foreach ($bonusesExist as $val) {
			$bonusesExistArr[] = $val->reasonId;
		}
		
		//select only the lead that exist in Bonus table
		$allBonusReasons = self::find()->where(['id'=>$bonussExistArr])->all();
		$allBonusReasonsArray = ArrayHelper::
					map($allBonusReasons, 'id', 'name');
	
		$allBonusReasonsArray[-1] = 'All BonusReasons';
		$allBonusReasonsArray = array_reverse ( $allBonusReasonsArray, true );
		return $allBonusReasonsArray;	
	}
}
