<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bonus".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $reasonId
 * @property integer $amount
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userId', 'reasonId', 'amount', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['id', 'userId', 'reasonId', 'amount', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'reasonId' => 'Reason ID',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public static function getBonuses()
	{
		$allBonuses = self::find()->all();
		$allBonusesArray = ArrayHelper::
					map($allBonuses, 'id', 'name');
		return $allBonusesArray;						
	}
	
     public function getBonusItem()
    {
        return $this->hasOne(BonusReason::className(), ['id' => 'reasonId']);
    }
}
