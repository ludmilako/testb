<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BonusReason */

$this->title = 'Create Bonus Reason';
$this->params['breadcrumbs'][] = ['label' => 'Bonus Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-reason-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
